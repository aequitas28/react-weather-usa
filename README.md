# Simple React + React-Redux Weather

This React project was created whilst learning React and uses a third party webservice which might cause issues.

This project allows the user to search up to 5 cities in the United States of America and retrieve weather details using an online weather API. 

### To get started:
1. Run command `npm install`
2. Run command `npm start`
    - This will start the dev server
3. Open browser and navigate to http://localhost:8080    

